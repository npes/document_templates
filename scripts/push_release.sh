#!/bin/bash

# see https://gitlab.com/gitlab-org/gitlab-ce/issues/31221#note_140029643
# and https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html


function push_file() {
	#echo "pushing $1"
	#echo "- log file: $2"

	curl --request POST --header "PRIVATE-TOKEN: $RELEASEAPIKEY" --form "file=@$1" $CI_API_V4_URL$UPLOAD_API_PATH > $2
	UPLOAD_URL=$(cat $2 | jq -r '.url')
	#echo "- url is $UPLOAD_URL"

	echo "$UPLOAD_URL"
}

set -e

export UPLOAD_API_PATH="/projects/$CI_PROJECT_ID/uploads"

# upload artifact to gitlab
SCRIPT_UPLOAD_URL=$(push_file doc_scripts-$CI_COMMIT_TAG.tgz upload_receipt_script.json)
echo "- script url $SCRIPT_UPLOAD_URL"

TEMPLATE_UPLOAD_URL=$(push_file ucl_template-$CI_COMMIT_TAG.tgz upload_receipt_template.json)
echo "- template url $TEMPLATE_UPLOAD_URL"


# build release.json
cat <<EOF > release.json
{
	"name": "$CI_COMMIT_TAG",
	"tag_name": "$CI_COMMIT_TAG",
	"description": "$CI_COMMIT_TITLE",
	"assets":
	{
		"links":
		[
			{ 	"name": "doc_scripts-$CI_COMMIT_TAG.tgz",
				  "url": " $CI_PROJECT_URL$SCRIPT_UPLOAD_URL"
			},
			{ 	"name": "ucl_template-$CI_COMMIT_TAG.tgz",
					"url": " $CI_PROJECT_URL$TEMPLATE_UPLOAD_URL"
			}
		]
	}
}

EOF

curl --header 'Content-Type: application/json'  \
     --header "PRIVATE-TOKEN: $RELEASEAPIKEY" \
     --data '@release.json' \
     --request POST $CI_API_V4_URL/projects/$CI_PROJECT_ID/releases | tee curl.log

FIRST_WORD=$(cat curl.log | cut -d ':' -f1)
echo ""
echo "First word of curl return value is $FIRST_WORD"
if [ "x$FIRST_WORD" == 'x{"message"' ]; then
  echo "return value starts with message, means failure...."
  exit 1
fi


# curl -v --header "PRIVATE-TOKEN: $RELEASEAPI" "https://gitlab.com/api/v4/projects/10599130/releases/v1.0.4/assets/links"
