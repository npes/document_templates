#!/usr/bin/env bash

if [ "x" == "x$1" ]; then
	echo "usage: $0 <pdf basename>"
	exit 1
fi

PDFNAME=$1
echo XYZ

# we have an issue with pdftohtml in certain versions of pdftohtml
UTILS_VERS=$(pdftohtml -v 2>&1 >/dev/null | head -n 1)
echo "pdftohtml version: $UTILS_VERS"
if [ "x_$UTILS_VERS" == "x_pdftohtml version 0.71.0" ]; then
	echo "Warning: malfunctioning pdftohtml, skipping"
	exit 0
fi

if ! pdftohtml $PDFNAME.pdf; then
	echo "Failure during conversion"
	exit 2
fi

if ! linkchecker $PDFNAME.html; then
	echo "Failure during linkcheck"
	exit 3
fi
