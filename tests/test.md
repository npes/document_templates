---
title: 'Template test'
subtitle: 'Markdown + Latex + pandoc'
authors: ['Author Name 1', 'Author Name 2']
main_author: 'Author Name 1'
date: \today
#institution: UCL University college
email: 'test@example.com'
left-header: \today
right-header: Template test
skip-toc: false
skip-lof: false
status: Test only
---

# Introduction

A simple document to test templates. Should be expanded to show how all markdown renders...

And a footnote ^[school link: [http://eal.dk](http://eal.dk)]

This is a list

* A
* B
    * sub B
* C

and some more text.

And this is a really really long line with lots of text and blabla to ensure that must be doing a certain amount of line wrapping. That was a lot.

# How does it handle strange characters?

* Danish letters: >æøåÆØÅ<
* Accents: >êéè<

# Image

We want images to be where they are mentioned in the text, like this

![this is a gitlab logo](logo.png){ height=25px }

# A bit about the preamble options

* title: 'Template test'

    Document title, also used in headers.

* subtitle: 'Markdown + Latex + pandoc'

    Document subtitle

* authors: ['Author Name 1', 'Author Name 2']

    This is written on the front page. May be omitted.

* main_author: 'Author Name 1'

    This is the contact person on the front page and used in footer.

* email: 'test@example.com'

    This is the email adress of the contact person on the front page and used in footer.

* date: \today

    The date of the document. Use `\today` to use current date.

* institution: Lillebaelt Academy (default: "UCL university college")

    For the front page. Don't change this, since it matches the logo.

* left-header: \today

    Header text to the left.

* right-header: Template test

    Header text to the right. Use document title if in doubt.


* skip-toc: false (Default: false)

    Skip table of content. Usable for short documents.

* skip-lof: false (Default: false)

    Skip list of figures. Usable for documents with few figures.

* status: draft (Default: empty)

    Show a watermark across all pages with the word written as status. Default is no watermark.
