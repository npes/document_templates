
# Tool for generating pdfs from markdown

In order to run this you need some additional programs. You can run the **install.sh** script, located in the /scripts folder.<br>
*It is recommended always to preview the script before installing it.<br>*
<br>Script is designed for Ubuntu/Debian based distributions. If you are using a different one, please install the missing programs manually.<br>

We are using Debian Stretch docker container in the GitLab CI. Its recommended you also use it as its more stable and there are no problems with the programs in use.

List of programs used:
*pandoc, linkchecker, texlive-latex-extra, texlive-latex-recommended, texlive-latex-base, poppler-utils*

After that just clone the repo and you are ready to start making some beautiful pdfs. You can clone the repo by copy/pasting this command: <br>

```git
git clone https://gitlab.com/moozer/document_templates.git
```

To create a pdf you have to put the .md file in the *tests* folder and run the command while inside the folder:

*template/scripts/build_pdf.sh* **name_of_file**(no need to include extension)

To test what you have created use:

*template/scripts/test_pdf.sh* **name_of_file**(no need to include extension)

# GitLab CI

This project uses the GitLab CI to generate the example pdfs.

There is a guide located [Here](https://moozer.gitlab.io/document_templates/index.html).

See [.gitlab-ci.yml](.gitlab-ci.yml) for more detailed information.