author:            Anatoli Penev
summary:           How to use PDF builder
id:                pdf_guide
categories:        static page
environments:      markdown
status:            final
feedback link:     https://gitlab.com/moozer/document_templates/issues
analytics account: 0

# How to build PDFs

## Overview of the tutorial

Duration: 1:00

This tutorial shows you how to build PDFs from markdown and have them online.

We use the test file [tests/test.md](../test.md). It also include formatting examples and which preamble options there are available.

The converted result i shown below.

![After convert_inside](pdf_after_convert.png)

It is also available as pdf, located [here](../test_custom_buster.pdf)

## Installing software

Duration: 10:00

If you use the docker image from [docker-pdf](https://gitlab.com/moozer/docker-pdf/container_registry) the software is already installed. This is the one we use ourselves, so it should be up-to-date.

Running it localy requires additional software to be installed. See the [dockerfile](https://gitlab.com/moozer/docker-pdf/blob/master/buster/Dockerfile) for the latest list.

As of now, the packages needed

```
apt-get install git pandoc \
        texlive-latex-extra texlive-latex-recommended texlive-latex-base \
        linkchecker poppler-utils
```


## Getting the scripts

Duration: 5:00

In order to use it, download the latest version of both the scripts and the UCL template.


E.g.
```
wget -O ucl_template-latest.tgz https://moozer.gitlab.io/document_templates/ucl_template-latest.tgz
mkdir -p template
tar xzvf ucl_template-latest.tgz -C template

wget -O doc_scripts-latest.tgz https://moozer.gitlab.io/document_templates/doc_scripts-latest.tgz
mkdir -p template/scripts
tar xzvf doc_scripts-latest.tgz -C template/scripts
```

They are other versionse available from the [releases page](https://gitlab.com/moozer/document_templates/-/releases).

Note: This replaces the previous method of using submodules. That method still works, but is considered obsolete.

## creating pdfs

Duration: 10:00

To convert the file `mydoc.md`, use

```
template/scripts/build_pdf.sh mydoc
template/scripts/test_pdf.sh mydoc
```

This will create `mydoc.pdf` and run the predefined tests. The test is currently linkchecking, which is disabled on some systems due to upstream bugs.


## GitLab CI

Duration: 5:00

The main idea of this project was to create PDFs automatically when `.md` files were updated, so that is a possibility.

In order to do this on gitlab, you need to edit the `.gitlab-ci.yml` for project and add something like the following to generate pdfs from all .md files in `/docs`

```yaml
build_pdfs_custom:
  image: registry.gitlab.com/moozer/docker-pdf/buster:latest
  before_script:
    - wget -O ucl_template-latest.tgz https://moozer.gitlab.io/document_templates/ucl_template-latest.tgz
    - mkdir -p template
    - tar xzvf ucl_template-latest.tgz -C template

    - wget -O doc_scripts-latest.tgz https://moozer.gitlab.io/document_templates/doc_scripts-latest.tgz
    - mkdir -p template/scripts
    - tar xzvf doc_scripts-latest.tgz -C template/scripts
  script:
    - for F in $(ls *.md); do
        template/scripts/build_pdf.sh $(basename -s .md $F);
        template/scripts/test_pdf.sh $(basename -s .md $F);
      done
  artifacts:
    paths:
      - *.pdf

```

This will convert all `.md` files in docs to `.pdf`, and store them as artifacts for jobs to come.

## A note on versions

We have a `latest` versions located at [ucl_template-latest.tgz](ucl_template-latest.tgz) and [doc_scripts-latest.tgz](doc_scripts-latest.tgz). These are the rememded version to use, since you will get any updates made to the template.

For the list of releases and their URLs go [here](https://gitlab.com/moozer/document_templates/-/releases)
